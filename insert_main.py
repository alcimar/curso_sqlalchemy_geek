from conf.db_session import create_session
from models.aditivo_nutritivo import AditivoNutritivo



def insert_aditivo_nutritivo() -> None:
    print('Cadastrando aditivo nutritivo')

    nome: str = input('Informe o nome do Aditivo Nutritivo: ')
    formula_quimica: str = input('Informe a fórmula química do Aditivo Nutritivo: ')

    aditivo_nutritivo = AditivoNutritivo(nome=nome, formula_quimica=formula_quimica)

    '''
    crimos um context manager e colocamos na variável de contexto de nome session
    '''
    with create_session() as session:
        ''''
        Como criamos um context manager tudo que estiver antes do commit será executado de uma vez
        '''
        session.add(aditivo_nutritivo)
        session.commit()

    print('Aditivo nutritivo cadastrado com sucesso')
    print(f'ID: {aditivo_nutritivo.id}')
    print(f'Data: {aditivo_nutritivo.data_criacao}')
    print(f'Nome: {aditivo_nutritivo.nome}')
    print(f'Fórmula Química: {aditivo_nutritivo.formula_quimica}')


if __name__ == '__main__':
    insert_aditivo_nutritivo()