from datetime import datetime
import sqlalchemy.orm as orm
from models.model_base import ModelBase
import sqlalchemy as sa
from models.revendedor import Revendedor
from typing import List
from models.lote import Lote
from sqlalchemy.orm import Mapped

# Nota Fiscal pode ter vários lotes
lotes_nota_fiscal = sa.Table(
    'lotes_nota_fiscal',
    ModelBase.metadata,
    sa.Column('id_nota_fiscal', sa.Integer, sa.ForeignKey('notas_fiscais.id')),
    sa.Column('id_lote', sa.Integer, sa.ForeignKey('lotes.id'))
)


class NotaFiscal(ModelBase):
    __tablename__: str = 'notas_fiscais'

    id: int = sa.Column(sa.BigInteger, primary_key=True, autoincrement=True)
    data_criacao: datetime = sa.Column(sa.DateTime, default=datetime.now, index=True)

    valor: float = sa.Column(sa.DECIMAL(8,2), nullable=False)
    numero_serie: str = sa.Column(sa.String(45), unique=True, nullable=False)
    descricao: str = sa.Column(sa.String(200), nullable=False)

    id_revendedor: int = sa.Column(sa.Integer, sa.ForeignKey('revendedores.id'))# tabela.campo
    revendedor: Mapped[List["Revendedor"]] = orm.relationship('Revendedor', lazy='joined')# Configuração interna do sqlalchemy

    # Uma nota fiscal pode ter vários lotes e um lote está ligado a uma nota fiscal
    lotes: Mapped[List["Lote"]] = orm.relationship('Lote', secondary=lotes_nota_fiscal, backref='lote', lazy='dynamic')

    def __repr__(self) -> str:
        return f'<Nota Fiscal: {self.nome}>'
