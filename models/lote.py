from datetime import datetime
import sqlalchemy.orm as orm
from models.model_base import ModelBase
import sqlalchemy as sa
from models.tipo_picole import TipoPicole
from sqlalchemy.orm import Mapped
from typing import List



class Lote(ModelBase):
    __tablename__: str = 'lotes'

    id: int = sa.Column(sa.BigInteger, primary_key=True, autoincrement=True)
    data_criacao: datetime = sa.Column(sa.DateTime, default=datetime.now, index=True)

    id_tipo_picole: int = sa.Column(sa.Integer, sa.ForeignKey('tipos_picole.id'))# tabela.campo
    tipo_picole:  Mapped[List["TipoPicole"]] = orm.relationship('TipoPicole', lazy='joined')# Configuração interna do sqlalchemy

    quantidade: int = sa.Column(sa.Integer, nullable=False)

    def __repr__(self) -> str:
        return f'<Lote: {self.nome}>'
