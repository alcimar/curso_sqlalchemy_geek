import sqlalchemy as sa
from sqlalchemy.orm import sessionmaker
from pathlib import Path
from typing import Optional
from sqlalchemy.orm import Session
from sqlalchemy.future.engine import Engine
from sqlalchemy import create_engine

from models.model_base import ModelBase

engine = create_engine("postgresql+psycopg2://postgres:123456@localhost/picole")

def create_session() -> Session:
    """
       Função para criar a sesssão de conexão do banco de dados
    """
    engine = create_engine("postgresql+psycopg2://postgres:123456@localhost/picole")


    __session = sessionmaker(engine, expire_on_commit=False, class_=Session)

    session: Session = __session()
    return session


def create_tables() -> None:

    import models.__all_models
    ModelBase.metadata.drop_all(engine)
    ModelBase.metadata.create_all(engine)
